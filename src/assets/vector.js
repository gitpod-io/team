var map;
if (mapboxgl.supported()) {
  initializeMap();
} else {
  document.querySelector('#status').innerHTML =
    "<span>Your browser doesn't support our Map." +
    '<br/>' +
    'We are sorry for the inconvenience, please try with another browser.</span>';
}

function initializeMap() {
  mapboxgl.accessToken =
    'pk.eyJ1IjoiZ3RzaW9saXMiLCJhIjoiY2pmOG55d3BoMnFidTMzcGt4aWh6ejAzeiJ9.R3DfGZ0OWR7JasWzEHFbuw';

  var MIN_ZOOM = 1.2;
  var MAX_ZOOM = 12;
  var FIVE_MIN = 5 * 60 * 1000;

  var TEAM_SRC_ID = 'team';
  var SUN_SRC_ID = 'sun-up';

  var self = this;
  var spiderifier;

  // objects for caching and keeping track of HTML marker objects (for performance)
  self.markers = {};
  self.markersOnScreen = {};

  map = new mapboxgl.Map({
    container: 'map',
    minZoom: MIN_ZOOM,
    maxZoom: MAX_ZOOM,
    style: 'mapbox://styles/mapbox/dark-v9?optimize=true',
    hash: false,
    bounds: [
      [-156, -45],
      [170, 64]
    ],
    fitBoundsOptions: { padding: 50 },
    pitchWithRotate: false,
    touchZoomRotate: false,
    renderWorldCopies: true,
    attributionControl: false,
    scrollZoom: false
  });

  map.on('load', function () {
    // add a clustered GeoJSON source for the team data
    map.addSource(TEAM_SRC_ID, {
      type: 'geojson',
      data: './team_geo.json',
      cluster: true,
      clusterRadius: 67
    });

    // circle and symbol layers for rendering individual team (unclustered points)
    map.addLayer({
      id: 'team_layer',
      type: 'symbol',
      source: TEAM_SRC_ID
    });

    // add a source for the sun status
    map.addSource(SUN_SRC_ID, { type: 'geojson', data: new Terminator() });

    // add a layer for the sun status
    map.addLayer({
      id: 'sun_up_layer',
      type: 'fill',
      source: SUN_SRC_ID,
      layout: {},
      paint: {
        'fill-color': '#111',
        'fill-opacity': 0.25
      }
    });
  });

  spiderifier = new MapboxglSpiderifier(map, {
    onClick: function (e, spiderLeg) {
      e.stopPropagation();
      createPopup(
        [spiderLeg.param.x, spiderLeg.param.y - 25],
        spiderLeg.mapboxMarker._lngLat,
        spiderLeg.feature
      );
    },
    circleSpiralSwitchover: Infinity,
    circleFootSeparation: 50,
    customPin: false,
    initializeLeg: function (spiderLeg) {
      var el = createMarker(spiderLeg.feature.picture);
      spiderLeg.elements.container.innerHTML = '';
      spiderLeg.elements.container.append(el);
    }
  });

  map.addControl(
    new mapboxgl.NavigationControl({
      showCompass: false
    }),
    'top-left'
  );

  function inIframe() {
    try {
      return window.self !== window.top;
    } catch (e) {
      return true;
    }
  }

  map.addControl(
    new mapboxgl.AttributionControl({
      customAttribution: inIframe() ? '<a href="" target="_blank">Full Screen</a>' : undefined
    })
  );

  map.on('error', function (error) {
    document.querySelector('#status').innerHTML =
      '<span>An error happened while loading the map.' +
      ' Please try again.' +
      '<br/>' +
      'If this error persists, please ' +
      '<a href="https://gitlab.com/gitlab-com/teampage-map/issues" target="_blank">file an issue</a>.</span>';
  });

  var timeout;

  function debouncedUpdateMarkers() {
    if (timeout) {
      window.cancelAnimationFrame(timeout);
    }

    // Setup the new requestAnimationFrame()
    timeout = window.requestAnimationFrame(function () {
      // Run our scroll functions
      updateMarkers();
    });
  }

  function showMarker(id, marker) {
    self.markersOnScreen[id] = true;
    marker.addTo(map);
  }

  function callback(coords, props, id, err, res) {
    var marker = self.markers[id];
    if (!marker) {
      var el = createCluster(props, res, coords);
      marker = self.markers[id] = new mapboxgl.Marker({ element: el }).setLngLat(coords);
    }
    showMarker(id, marker);
  }

  function updateMarkers() {
    var features = map.querySourceFeatures(TEAM_SRC_ID);

    var visibleIds = features.map(function (x) {
      if (x.properties && x.properties.cluster) {
        return x.properties.cluster_id;
      }
      return x.properties && x.properties.picture;
    });

    // for every marker we've added previously, remove those that are no longer visible
    for (id in self.markersOnScreen) {
      if (self.markersOnScreen.hasOwnProperty(id) && !visibleIds.includes(parseInt(id, 10))) {
        self.markers[id].remove();
        delete self.markersOnScreen[id];
      }
    }

    // for every cluster on the screen, create an HTML marker for it (if we didn't yet),
    // and add it to the map if it's not there already
    for (var i = 0; i < features.length; i++) {
      var coords = features[i].geometry.coordinates;

      var props = features[i].properties;

      var id = props.cluster ? props.cluster_id : props.picture;

      if (self.markersOnScreen[id] || coords[0] < -180 || coords[0] > 180) {
        continue;
      }

      if (self.markers[id]) {
        showMarker(id, self.markers[id]);
      } else if (props.cluster) {
        map
          .getSource(TEAM_SRC_ID)
          .getClusterLeaves(id, 4, 0, callback.bind(self, coords, props, id));
      } else {
        var el = createMarker(props.picture);

        el.addEventListener('click', createPopup.bind(self, 25, coords, props));

        var marker = (self.markers[id] = new mapboxgl.Marker({ element: el }).setLngLat(coords));

        showMarker(id, marker);
      }
    }
  }

  function getClusterExpansionZoom(clusterId) {
    return new Promise(function (resolve, reject) {
      map.getSource(TEAM_SRC_ID).getClusterExpansionZoom(clusterId, function (err, zoom) {
        if (err) {
          return reject(err);
        }

        var newZoom = zoom - map.getZoom() < 1.5 ? zoom + 1.5 : zoom + 0.1;

        if (newZoom < MIN_ZOOM) {
          newZoom = MIN_ZOOM;
        } else if (newZoom > MAX_ZOOM) {
          newZoom = MAX_ZOOM;
        }

        return resolve(newZoom);
      });
    });
  }

  function getClusterChildren(props) {
    return new Promise(function (resolve, reject) {
      map
        .getSource(TEAM_SRC_ID)
        .getClusterLeaves(props.cluster_id, props.point_count, 0, function (err, children) {
          if (err) {
            return reject(err);
          }

          return resolve(children);
        });
    });
  }

  function allInOnePlace(markers) {
    return (
      markers
        .map(function (marker) {
          return marker.geometry.coordinates.join(':');
        })
        .filter(function (e, i, arr) {
          return arr.lastIndexOf(e) === i;
        }).length < 3
    );
  }

  // code for creating an SVG donut chart from feature properties
  function createCluster(props, res, coords) {
    var imgs = res.map(function (response) {
      return '<img src="' + response.properties.picture + '"/>';
    });

    var count = '<div class="count">' + props.point_count + '</div>';

    var html = '<div class="group-' + imgs.length + '">' + imgs.join('\n') + count + '</div>';

    var el = document.createElement('div');
    el.innerHTML = html;
    var firstChild = el.firstChild;

    firstChild.addEventListener('click', clusterClick.bind(null, props, coords, firstChild));
    return firstChild;
  }

  function createMarker(picture) {
    var el = document.createElement('img');
    el.src = picture;
    return el;
  }

  function reRenderSun() {
    map.getSource(SUN_SRC_ID).setData(new Terminator());
  }

  // after the GeoJSON data is loaded, update markers on the screen
  // and do so on every map move/moveend
  map.on('data', function (e) {
    if (e.sourceId !== TEAM_SRC_ID || !e.isSourceLoaded || e.sourceDataType === 'metadata') {
      return;
    }
    document.querySelector('#status').remove();

    map.on('move', debouncedUpdateMarkers);
    map.on('moveend', debouncedUpdateMarkers);
    map.on('movestart', debouncedUpdateMarkers);
    debouncedUpdateMarkers();

    setInterval(reRenderSun, FIVE_MIN);
  });

  var popup;

  function createPopup(offset, latLng, props, event) {
    map.panTo(latLng);
    if (event && event.stopPropagation) {
      event.stopPropagation();
    }

    if (popup && popup.remove) {
      popup.remove();
    }

    var name = '<div class="member-name">' + props.name + '</div>';

    var location = [
      '<div class="member-location">',
      props.name.includes('Ablett') ? '<marquee width="200">' : '',
      props.locality ? props.locality : '',
      props.stateCode ? ', ' + props.stateCode : '',
      props.locality || props.stateCode ? ' &middot; ' : '',
      props.country,
      props.name.includes('Ablett') ? '</marquee>' : '',
      '</div>'
    ].join('');

    console.log(location);

    popup = new mapboxgl.Popup({
      offset: offset
    })
      .setLngLat(latLng)
      .setHTML(name + location)
      .addTo(map);
  }

  var spidered = null;

  function clusterClick(props, coords, el) {
    getClusterChildren(props).then(function (children) {
      var zoom = map.getZoom();
      var count = props.point_count;

      if ((zoom < 6 || count > 10) && !allInOnePlace(children)) {
        getClusterExpansionZoom(props.cluster_id).then(function (newZoom) {
          map.flyTo({ zoom: newZoom, center: coords });
          debouncedUpdateMarkers.call(self);
        });
      } else {
        map.panTo(coords);
        var markers = children.map(function (leafFeature) {
          return leafFeature.properties;
        });
        spidered = el;
        el.classList.add('hidden');
        spiderifier.spiderfy(coords, markers);
      }
    });
  }

  var registeredHandler = false;

  function registerScrollDisableHandler() {
    function disable() {
      map.scrollWheelZoom.disable();
      map.off('blur');
      map.off('mouseout');
      registeredHandler = false;
      registerScrollEnableHandler();
    }

    map.on('blur', disable);

    map.on('mouseout', disable);
  }

  function registerScrollEnableHandler() {
    if (registeredHandler) {
      return;
    }

    registeredHandler = true;

    map.on('click', function () {
      map.scrollZoom.enable();
      map.off('click');
      registerScrollDisableHandler();
    });
  }

  map.on('click', function () {
    if (spidered) {
      spidered.classList.remove('hidden');
    }
    spiderifier.unspiderfy();
    map.scrollZoom.enable();

    registerScrollEnableHandler();
  });
}
